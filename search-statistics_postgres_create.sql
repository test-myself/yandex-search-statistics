CREATE TABLE "Domain" (
	"id" serial(6) NOT NULL,
	"name" char(50) NOT NULL UNIQUE,
	CONSTRAINT "Domain_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "LogEvent" (
	"id" serial(6) NOT NULL,
	"domain_id" int(6) NOT NULL,
	"querytag_id" int(6) NOT NULL,
	"searchevent_id" int(6) NOT NULL,
	CONSTRAINT "LogEvent_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "QueryTag" (
	"id" serial(6) NOT NULL,
	"name" char(50) NOT NULL UNIQUE,
	CONSTRAINT "QueryTag_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "SearchEvent" (
	"id" serial(6) NOT NULL,
	"timedate" TIMESTAMP,
	"searchurl" char(300) NOT NULL,
	CONSTRAINT "SearchEvent_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

ALTER TABLE "LogEvent" ADD CONSTRAINT "LogEvent_fk0" FOREIGN KEY ("domain_id") REFERENCES "Domain"("id");
ALTER TABLE "LogEvent" ADD CONSTRAINT "LogEvent_fk1" FOREIGN KEY ("querytag_id") REFERENCES "QueryTag"("id");
ALTER TABLE "LogEvent" ADD CONSTRAINT "LogEvent_fk2" FOREIGN KEY ("searchevent_id") REFERENCES "SearchEvent"("id");


CREATE OR REPLACE VIEW public."LastSearchDomainCount"
 AS
select query.name as Query, domain.name as Domain, count(event.*) as Count, max(search.timedate) as timedate
from public."LogEvent" as event
join public."QueryTag" as query on query.id = event.querytag_id
join public."Domain" as domain on domain.id = event.domain_id
join public."SearchEvent" as search on search.id = event.searchevent_id
group by query.name, domain.name;

ALTER TABLE public."LastSearchDomainCount"
    OWNER TO postgres;

CREATE OR REPLACE VIEW public."QueryDomainCount"
 AS
select query.name as Query, domain.name as Domain, count(event.*) as Count
from public."LogEvent" as event
join public."QueryTag" as query on query.id = event.querytag_id
join public."Domain" as domain on domain.id = event.domain_id
group by query.name, domain.name;

ALTER TABLE public."QueryDomainCount"
    OWNER TO postgres;
