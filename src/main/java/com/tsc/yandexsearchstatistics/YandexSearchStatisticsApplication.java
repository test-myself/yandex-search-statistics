package com.tsc.yandexsearchstatistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YandexSearchStatisticsApplication {

	public static void main(String[] args) {
		SpringApplication.run(YandexSearchStatisticsApplication.class, args);
	}

}
