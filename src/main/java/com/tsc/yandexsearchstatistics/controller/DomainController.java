package com.tsc.yandexsearchstatistics.controller;

import ch.qos.logback.core.net.server.Client;
import com.tsc.yandexsearchstatistics.model.Domain;
import com.tsc.yandexsearchstatistics.service.DomainService;
import com.tsc.yandexsearchstatistics.service.DomainServiceImpl;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DomainController {

    private final DomainServiceImpl domainService;

    @Autowired
    public DomainController(DomainServiceImpl domainService) {
        this.domainService = domainService;
    }

    @PostMapping(value = "/domains/{name}")
    public ResponseEntity<?> create(@PathVariable(name = "name") String name) {
        Domain domain = new Domain(name);
        domainService.create(domain);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/domains")
    public ResponseEntity<List<Domain>> read() {
        final List<Domain> domains = domainService.findAll();

        return domains != null &&  !domains.isEmpty()
                ? new ResponseEntity<>(domains, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/showDomains")
    public String findDomains(Model model) {
        var domains = (List<Domain>) domainService.findAll();

        model.addAttribute("domains", domains);

        return "showCities";
    }

    @PutMapping(value = "/domains/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") int id, @RequestBody Domain domain) {
        final boolean updated = domainService.update(domain);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/domains/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") int id) {
        final boolean deleted = domainService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}
