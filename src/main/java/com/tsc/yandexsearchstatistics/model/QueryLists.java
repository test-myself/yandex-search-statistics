package com.tsc.yandexsearchstatistics.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class QueryLists {

    List<QueryTag> queryTags = new ArrayList<>();
    List<QueryTag> uniqueTags = new ArrayList<>();
    List<QueryTag> existedTags = new ArrayList<>();

    void addToQueryTags (QueryTag queryTag) {
        queryTags.add(queryTag);
    }

    public void addToUniqueTags (QueryTag queryTag) {
        uniqueTags.add(queryTag);
        this.addToQueryTags(queryTag);
    }

    public void addToExistedTags (QueryTag queryTag) {
        existedTags.add(queryTag);
        this.addToQueryTags(queryTag);
    }

}
