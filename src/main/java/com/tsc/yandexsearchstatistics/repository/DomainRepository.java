package com.tsc.yandexsearchstatistics.repository;

import  com.tsc.yandexsearchstatistics.model.Domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DomainRepository extends CrudRepository<Domain, Integer> {
    
}
