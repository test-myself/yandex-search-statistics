package com.tsc.yandexsearchstatistics.repository;

import com.tsc.yandexsearchstatistics.model.QueryTag;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface QueryTagRepository extends CrudRepository<QueryTag, Integer> {

    @Query("SELECT 1 FROM public.query_tag q WHERE :name = q.name")
    int checkExistence(@Param("name") String name);


}