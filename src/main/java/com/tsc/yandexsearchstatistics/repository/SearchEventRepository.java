package com.tsc.yandexsearchstatistics.repository;

import com.tsc.yandexsearchstatistics.model.SearchEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchEventRepository extends CrudRepository<SearchEvent, Integer> {
}