package com.tsc.yandexsearchstatistics.service;

import com.tsc.yandexsearchstatistics.model.LogEvent;

import java.util.List;
import java.util.Optional;

public interface LogEventService {

    void create(LogEvent logEvent);

    List<LogEvent> findAll();

    Optional<LogEvent> read(LogEvent logEvent);

    boolean update(LogEvent logEvent);

    boolean delete(int id);
    
}
