package com.tsc.yandexsearchstatistics.service;

import com.tsc.yandexsearchstatistics.model.LogEvent;
import com.tsc.yandexsearchstatistics.repository.LogEventRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LogEventServiceImpl implements LogEventService {

    @Autowired
    private LogEventRepository repository;

    @Override
    public void create(LogEvent logEvent) {
        repository.save(logEvent);
    }

    @Override
    public List<LogEvent> findAll() {
        var logEvent = (List<LogEvent>) repository.findAll();

        return logEvent;
    }

    @Override
    public Optional<LogEvent> read(LogEvent logEvent) {
        return repository.findById(logEvent.getId());
    }

    @Override
    public boolean update(LogEvent logEvent) {
        try {
            repository.save(logEvent);
        } catch (Exception exception) {
            return true;
        }

        return true;
    }

    @Override
    public boolean delete(int id) {
        try {
            repository.deleteById(id);
        } catch (Exception exception) {
            return true;
        }

        return true;
    }
}
