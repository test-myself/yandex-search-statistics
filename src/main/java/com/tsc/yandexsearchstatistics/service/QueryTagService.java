package com.tsc.yandexsearchstatistics.service;

import com.tsc.yandexsearchstatistics.model.QueryTag;

import java.util.List;
import java.util.Optional;

public interface QueryTagService {

    void create(QueryTag queryTag);

    List<QueryTag> findAll();

    Optional<QueryTag> read(QueryTag queryTag);

    boolean update(QueryTag queryTag);

    boolean delete(int id);

    boolean checkExistence(QueryTag queryTag);

}
