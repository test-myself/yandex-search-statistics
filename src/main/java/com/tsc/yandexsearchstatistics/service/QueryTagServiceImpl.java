package com.tsc.yandexsearchstatistics.service;

import com.tsc.yandexsearchstatistics.model.QueryTag;
import com.tsc.yandexsearchstatistics.repository.QueryTagRepository;
import lombok.var;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class QueryTagServiceImpl implements QueryTagService{

    @Autowired
    private QueryTagRepository repository;

    @Override
    public void create(QueryTag queryTag) {
        repository.save(queryTag);
    }

    @Override
    public List<QueryTag> findAll() {
        var queryTag = (List<QueryTag>) repository.findAll();

        return queryTag;
    }

    @Override
    public Optional<QueryTag> read(QueryTag queryTag) {
        return repository.findById(queryTag.getId());
    }

    @Override
    public boolean update(QueryTag queryTag) {
        try {
            repository.save(queryTag);
        } catch (Exception exception) {
            return true;
        }

        return true;
    }

    @Override
    public boolean delete(int id) {
        try {
            repository.deleteById(id);
        } catch (Exception exception) {
            return true;
        }

        return true;
    }

    @Override
    public boolean checkExistence(QueryTag queryTag) {
        final int checkTag = repository.checkExistence(queryTag.getName());

        if (checkTag != 0) return true;
        else return false;
    }


}
