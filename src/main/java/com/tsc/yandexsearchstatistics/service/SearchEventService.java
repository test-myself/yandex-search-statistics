package com.tsc.yandexsearchstatistics.service;

import com.tsc.yandexsearchstatistics.model.SearchEvent;

import java.util.List;
import java.util.Optional;

public interface SearchEventService {

    void create(SearchEvent searchEvent);

    List<SearchEvent> findAll();

    Optional<SearchEvent> read(SearchEvent searchEvent);

    boolean update(SearchEvent searchEvent);

    boolean delete(int id);
    
}
